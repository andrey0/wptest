<?php

/**
 * Стили.
 */
add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'main-style', get_stylesheet_directory_uri() . '/assets/css/style.css' );
} );

add_action( 'init', function () {

	// ЧПУ для продуктов и категорий.
	add_rewrite_rule( '^products/sepulki/([^/]*)/?$', 'index.php?sepulki_tax=$matches[1]', 'top' );
	add_rewrite_rule( '^products/sepulki/([^/]*)/?$', 'index.php?sepulki_tax=$matches[1]', 'top' );
	add_rewrite_rule( '^products/(sepulki)/?$', 'index.php?sepulki_tax=$matches[1]', 'top' );
	add_rewrite_rule( '^products/sepulki/([^/]*)/([^/]*)/?$', 'index.php?sepulki=$matches[2]', 'top' );
	add_rewrite_rule( '^products/(brenchalki)/?$', 'index.php?brenchalki_tax=$matches[1]', 'top' );
	add_rewrite_rule( '^products/brenchalki/([^/]*)?$', 'index.php?brenchalki_tax=$matches[1]', 'top' );
	add_rewrite_rule( '^products/brenchalki/([^/]*)/([^/]*)/?$', 'index.php?brenchalki=$matches[2]', 'top' );

	register_taxonomy( 'sepulki_tax', [ 'sepulki' ], [
		'label'             => 'Категории',
		'labels'            => array(
			'name'              => 'Категории сепулек',
			'singular_name'     => 'Категория',
			'search_items'      => 'Искать категорию',
			'all_items'         => 'Все категории',
			'parent_item'       => 'Родит. категория',
			'parent_item_colon' => 'Родит. категория:',
			'edit_item'         => 'Ред. категорию',
			'update_item'       => 'Обновить категорию',
			'add_new_item'      => 'Добавить категорию',
			'new_item_name'     => 'Новая категория',
			'menu_name'         => 'Категории',
		),
		'description'       => 'Рубрики для раздела сепульки',
		'public'            => true,
		'show_in_nav_menus' => false,
		'show_ui'           => true,
		'show_tagcloud'     => false,
		'hierarchical'      => true,
		'rewrite'           => array(
			'slug'         => 'products',
			'hierarchical' => true,
			'with_front'   => true,
			'feed'         => false
		),
		'show_admin_column' => true,
	] );

	register_taxonomy( 'brenchalki_tax', [ 'brenchalki' ], [
		'label'             => 'Категории',
		'labels'            => array(
			'name'              => 'Категории бренчалок',
			'singular_name'     => 'Категория',
			'search_items'      => 'Искать категорию',
			'all_items'         => 'Все категории',
			'parent_item'       => 'Родит. категория',
			'parent_item_colon' => 'Родит. категория:',
			'edit_item'         => 'Ред. категорию',
			'update_item'       => 'Обновить категорию',
			'add_new_item'      => 'Добавить категорию',
			'new_item_name'     => 'Новая категория',
			'menu_name'         => 'Категории',
		),
		'description'       => 'Рубрики для раздела бренчалки',
		'public'            => true,
		'show_in_nav_menus' => false,
		'show_ui'           => true,
		'show_tagcloud'     => false,
		'hierarchical'      => true,
		'rewrite'           => array(
			'slug'         => 'products',
			'hierarchical' => true,
			'with_front'   => true,
			'feed'         => false
		),
		'show_admin_column' => true,
	] );

	register_post_type( 'sepulki', array(
		'labels'             => array(
			'name'               => 'Сепульки',
			'singular_name'      => 'Сепулька',
			'add_new'            => 'Добавить новую',
			'add_new_item'       => 'Добавить новую сепульку',
			'edit_item'          => 'Редактировать сепульку',
			'new_item'           => 'Новая книга',
			'view_item'          => 'Посмотреть сепульку',
			'search_items'       => 'Найти сепульку',
			'not_found'          => 'Книг не найдено',
			'not_found_in_trash' => 'В корзине книг не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Сепульки'

		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'sepulki', 'with_front' => false ),
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'         => array( 'sepulki_tax' ),
	) );

	register_post_type( 'brenchalki', array(
		'labels'             => array(
			'name'               => 'Бренчалки',
			'singular_name'      => 'Бренчалка',
			'add_new'            => 'Добавить новую',
			'add_new_item'       => 'Добавить новую бренчалку',
			'edit_item'          => 'Редактировать бренчалку',
			'new_item'           => 'Новая книга',
			'view_item'          => 'Посмотреть бренчалку',
			'search_items'       => 'Найти бренчалку',
			'not_found'          => 'Книг не найдено',
			'not_found_in_trash' => 'В корзине книг не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Бренчалки'

		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'brenchalki', 'with_front' => false ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'         => array( 'brenchalki_tax' ),
	) );

} );

/**
 * ЧПУ для сепулек и бренчалок.
 */
add_filter( 'post_type_link', 'products_permalink', 1, 2 );
function products_permalink( $permalink, $post ) {
	$type = '';

	if (in_array($post->post_type, array('brenchalki', 'sepulki'))) {
		$type = $post->post_type;
	}
	else {
		return $permalink;
	}

	$terms = get_the_terms( $post, $type . '_tax' );
	if ( ! is_wp_error( $terms ) && ! empty( $terms ) && is_object( $terms[0] ) ) {
		$taxonomy_slug = get_term_parents_list( $terms[0]->term_id, $type . '_tax', array(
			'separator' => '/',
			'format'    => 'slug',
			'link'      => false,
			'inclusive' => true,
		) );
		$taxonomy_slug = trim( $taxonomy_slug, '/' );
	} else {
		$taxonomy_slug = 'categories';
	}

	return str_replace($type, 'products/' . $taxonomy_slug, $permalink );
}

/**
 * Параметры для блока ТОР-3 в админке на странице - Настройки публикации.
 */
add_action( 'admin_init', 'eg_settings_api_init' );
function eg_settings_api_init() {

	add_settings_section(
		'eg_setting_section',
		'Заголовок для секции настроек',
		'eg_setting_section_callback_function',
		'reading'
	);

	add_settings_section(
		'eg_setting_section',
		'Топ продуктов',
		'eg_setting_section_callback_function',
		'writing'
	);

	add_settings_field(
		'eg_setting_type',
		'Тип продукта',
		'eg_setting_type_callback_function',
		'writing',
		'eg_setting_section'
	);

	add_settings_field(
		'eg_setting_cat',
		'Категория продукта',
		'eg_setting_cat_callback_function',
		'writing', // страница
		'eg_setting_section'
	);

	register_setting( 'writing', 'eg_setting_type' );
	register_setting( 'writing', 'eg_setting_cat' );
}

/**
 * Описание блоба ТОР-3 в админке.
 */
function eg_setting_section_callback_function() {
	echo '<p>Блок топ продуктов на главной странице</p>';
}

/**
 * Кастомный селект бля типов продукта в блоке ТОР-3.
 */
function eg_setting_type_callback_function() {
	$args       = array(
		'public'   => true,
		'_builtin' => false
	);
	$output     = 'names';
	$operator   = 'and';
	$post_types = get_post_types( $args, $output, $operator );

	if ( ! $post_types ) {
		return;
	}

	$selected = get_option( 'eg_setting_type' );

	$out = '<select name="eg_setting_type">';

	foreach ( $post_types as $post_type ) {
		$checked = $selected == $post_type ? 'selected' : '';
		$out     .= '<option ' . $checked . ' value="' . $post_type . '">' . get_post_type_name($post_type) . '</option>';
	}

	$out .= '</select>';
	echo $out;
}

/**
 * Селект Категория продукта в блоке ТОР-3.
 */
function eg_setting_cat_callback_function() {
	wp_dropdown_categories(
		array(
			'hide_empty'   => 0,
			'name'         => 'eg_setting_cat',
			'orderby'      => 'name',
			'selected'     => get_option( 'eg_setting_cat' ),
			'hierarchical' => true,
			'taxonomy'     => [ 'brenchalki_tax', 'sepulki_tax' ],
		)
	);
}

/**
 * Пункты меню для сепулек и бернчалок.
 */
add_filter( 'wp_nav_menu_items', function ( $menu, $args ) {

	$args       = array(
		'public'   => true,
		'_builtin' => false
	);
	$output     = 'names';
	$operator   = 'and';
	$post_types = get_post_types( $args, $output, $operator );
	$_menu      = '';

	foreach ( $post_types as $post_type ) {
		$terms = get_terms( array( 'taxonomy' => $post_type . '_tax', 'childless' => true, ) );
		$_menu .= '<li class="menu-item menu-item-type-post_type menu-item-object-sepulki current-menu-item menu-item-has-children"><a href="/products/' . str_replace( "_post", "", $post_type ) . '">' . get_post_type_name( $post_type ) . '</a>
<button class="sub-menu-toggle" aria-expanded="false" onclick="twentytwentyoneExpandSubMenu(this)"><span class="icon-plus"><svg class="svg-icon" width="18" height="18" aria-hidden="true" role="img" focusable="false" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M18 11.2h-5.2V6h-1.6v5.2H6v1.6h5.2V18h1.6v-5.2H18z" fill="currentColor"></path></svg></span><span class="icon-minus"><svg class="svg-icon" width="18" height="18" aria-hidden="true" role="img" focusable="false" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M6 11h12v2H6z" fill="currentColor"></path></svg></span><span class="screen-reader-text">Открыть меню</span></button>
<ul class="sub-menu">';

		foreach ( $terms as $term ) {
			$posts = get_posts( array(
				'numberposts' => - 1,
				'tax_query'   => array(
					array(
						'taxonomy' => $term->taxonomy,
						'field'    => 'term_id',
						'terms'    => $term->term_id
					)
				),
				'post_type'   => $post_type,
			) );
			foreach ( $posts as $post ) {
				$_menu .= '<li><a href="' . get_term_link( $term->term_id ) . '">' . $term->name . '</a> - <a href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a></li>';
			}
		}

		$_menu .= '</li></ul>';
	}

	return $menu . $_menu;
}, 10, 2 );

/**
 * Хлебные крошки.
 */
function breadcrumbs() {
	global $post;

	$breadcrumbs = [];
	$separator   = ' / ';

	if ( is_single() ) {
		if ( in_array( $post->post_type, [ 'brenchalki', 'sepulki' ] ) ) {
			$breadcrumbs[] = '<a href="' . site_url() . '">Главная</a>';
			$breadcrumbs[] = '<span>Продукты</span>';
			$term_list     = wp_get_post_terms( $post->ID, $post->post_type . '_tax' );
			$_term         = reset( $term_list );
			if ( $_term->parent > 0 ) {
				$p_term        = get_term( $_term->parent );
				$breadcrumbs[] = "<a href='" . get_category_link( $p_term->term_id ) . "'>$p_term->name</a>";
				$breadcrumbs[] = "<a href='" . get_category_link( $_term->term_id ) . "'>$_term->name</a>";
			}

			$breadcrumbs[] = '<span>' . $post->post_title . '</span>';
		}
	}

	if ( is_archive() ) {

		$breadcrumbs[] = '<a href="' . site_url() . '">Главная</a>';
		$breadcrumbs[] = '<span>Продукты</span>';

		$_term = get_queried_object();
		if ( $_term->parent > 0 ) {
			$p_term        = get_term( $_term->parent );
			$breadcrumbs[] = "<a href='" . get_category_link( $p_term->term_id ) . "'>$p_term->name</a>";
			$breadcrumbs[] = "<a href='" . get_category_link( $_term->term_id ) . "'>$_term->name</a>";
		} else {
			$breadcrumbs[] = "<a href='" . get_category_link( $_term->term_id ) . "'>$_term->name</a>";
		}
	}

	echo '<div class="wrap-content">' . implode( $separator, $breadcrumbs ) . '</div>';

}

/**
 * @param $type
 *
 * @return mixed
 *
 * Название типа продукта.
 */
function get_post_type_name( $type ) {
	$post_type_obj = get_post_type_object( $type );
	return $post_type_obj->labels->name;
}
