<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();
?>

<?php

$eg_setting_type = get_option( 'eg_setting_type' );
$eg_setting_cat = get_option( 'eg_setting_cat' );
$term = get_term( $eg_setting_cat );

$the_query = new WP_Query( array(
	'posts_per_page' => 3,
	'post_type' => $eg_setting_type,
	'post_status' => 'publish',
	'meta_key' => 'top_list',
	'meta_value' => '1',
	'tax_query'   => array(
		array(
			'taxonomy' => $term->taxonomy,
			'field'    => 'term_id',
			'terms'    => $term->term_id
		)
	),
));

$myposts = $the_query->get_posts();
?>
    <?php
        $top3_title = "Топ " . get_post_type_name($eg_setting_type) . " в категории $term->name";
        echo "<h2>$top3_title</h2>";
    ?>
    <div class="products-wrap">

        <?php
        foreach( $myposts as $post ){ setup_postdata($post); ?>

            <div class="product-item">
                <div class="product-title"><a href="<?php echo get_permalink($post); ?>"><?php echo get_the_title(); ?></a></div>
                <div class="product-img"><a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post->ID, 'thumbnail'); ?></a></div>

                <?php
                    $field = '';
                    switch ($post->post_type) {
                        case 'brenchalki':
	                        $value = get_field( 'volume', $post->ID );
	                        $field = $value > 0 ? "$value децибел" : '';
                            break;
                        case 'sepulki':
	                        $value = get_field( 'size', $post->ID );
	                        $field = $value > 0 ? "$value метров" : '';
                            break;
                    }
                ?>

                <div class="product-field"><?php echo $field ?></div>
                <?php
                    $term_list = wp_get_post_terms( $post->ID, $post->post_type . '_tax');
                    $_term = reset($term_list);
                    echo "<a href='".get_category_link( $_term->term_id )."'>$_term->name</a>"
                ?>

            </div>

            <?php
            wp_reset_postdata();
        }
        ?>
    </div>
<?php
get_footer();
