<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();


?>

<?php
    $name = "";
    $_term = get_queried_object();
    if ($_term->parent > 0) {
        $p_term = get_term( $_term->parent );
        $name = "$p_term->name в категории $_term->name";
    }
    else {
        $name = "$_term->name всех категорий";
    }
?>
<?php breadcrumbs();?>

<div id="content" class="site-content">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <article id="post-1" class="post type-post">

                <header class="entry-header alignwide">

                    <h1 class="entry-title"><?php echo $name; ?></h1>
                </header><!-- .entry-header -->

                <div class="entry-content">
                    <?php
                        $categories = get_terms( array(
                            'orderby' => 'name',
                            'parent'  => $_term->term_id,
                            'taxonomy' => $_term->taxonomy,
                        ) );
                    ?>
                    <div class="category-list">
                        <ul>
                            <?php foreach ($categories as $category): ?>
                            <li><a href="<?php echo get_category_link( $category->term_id ) ?>"><?php echo $category->name;?></a></li>
                            <?php endforeach;?>
                        </ul>
                    </div>

                    <?php
                    $query = new WP_Query( array(
	                    'numberposts' => - 1,
	                    'tax_query'   => array(
		                    array(
			                    'taxonomy' => $_term->taxonomy,
			                    'field'    => 'term_id',
			                    'terms'    => $_term->term_id
		                    )
	                    ),
                    ) );

                    ?>

                    <div class="products-wrap">
		                <?php
		                foreach( $query->get_posts() as $post ){ setup_postdata($post); ?>

                            <div class="product-item">
                                <div class="product-title"><a href="<?php echo get_permalink($post); ?>"><?php echo get_the_title(); ?></a></div>
                                <div class="product-img"><a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post->ID, 'thumbnail'); ?></a></div>

				                <?php
				                $field = '';
				                switch ($post->post_type) {
					                case 'brenchalki':
						                $value = get_field( 'volume', $post->ID );
						                $field = $value > 0 ? "$value децибел" : '';
						                break;
					                case 'sepulki':
						                $value = get_field( 'size', $post->ID );
						                $field = $value > 0 ? "$value метров" : '';
						                break;
				                }
				                ?>

                                <div class="product-field"><?php echo $field ?></div>
				                <?php
				                $term_list = wp_get_post_terms( $post->ID, $post->post_type . '_tax');
				                $_term = reset($term_list);
				                echo "<a href='".get_category_link( $_term->term_id )."'>$_term->name</a>"
				                ?>

                            </div>

			                <?php
			                wp_reset_postdata();
		                }
		                ?>
                    </div>

                </div><!-- .entry-content -->
            </article><!-- #post-1 -->
        </main><!-- #main -->
    </div><!-- #primary -->
</div>

<?php get_footer(); ?>
